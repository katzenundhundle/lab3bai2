// ignore_for_file: unnecessary_this

class ImageRef {
  int? albumId;
  int? id;
  String? title;
  String? url;
  String? thumbnailUrl;
  ImageRef(this.albumId, this.id, this.title, this.url, this.thumbnailUrl);

  factory ImageRef.fromJson(dynamic json) {
    return ImageRef(
        json['albumId'] as int,
        json['id'] as int,
        json['title'] as String,
        json['url'] as String,
        json['thumbnailUrl'] as String);
  }

  @override
  String toString() {
    return '{ ${this.albumId}, ${this.id} }';
  }
}



// void main(List<String> args) {
//   fetchImage();
// }
// void processResponse(String jsonString) {
//   for (final jsonObject in json.decode(jsonString)) {
//     imageList.add(Image.fromJson(jsonObject));
//   }
// }
