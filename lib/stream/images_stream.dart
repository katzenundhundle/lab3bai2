// import "package:flutter/material.dart";
// import "../model/image_model.dart" as im;
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;

import '../model/image_model.dart';

final List<ImageRef> list = [];

class ImagesStream {
  Stream<ImageRef> getImages() async* {
    fetchImage();
    yield* Stream.periodic(Duration(seconds: 3), (int t) {
      int index = t;
      // print(list[index].title);
      return list[index];
    });
  }
}

fetchImage() async {
  print("oke");
  final response =
      await http.get(Uri.parse("https://jsonplaceholder.typicode.com/photos"));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.

    for (final jsonObject in json.decode(response.body)) {
      // print(response.body);
      list.add(ImageRef.fromJson(jsonObject));
      // print(list[list.length - 1].title);
    }
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }

  // return list;
}

void main(List<String> args) {
  ImagesStream imagesStream = ImagesStream();
  imagesStream.getImages().listen((event) {
    print(event.title);
  });
  // print(list);
}

// List<ImageRef> images = [];

//   fetchImages() async {
//     counter++;
//     var url = Uri.https('jsonplaceholder.typicode.com', 'photos/$counter');
//     var response = await http.get(url);

//     print(response.body);

//     var jsonObject = json.decode(response.body);
//     var imageRef = ImageRef(jsonObject['id'], jsonObject['url']);
//     images.add(imageModel);


//     print('counter=$counter');
//     print('Length of images=${images.length}');

//     setState(() {
//       // Do nothing
//     });
//   }