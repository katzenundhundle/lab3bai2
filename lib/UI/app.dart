import 'dart:async';
import 'dart:html';

import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
import '../model/image_model.dart';
import '../stream/images_stream.dart';
// import '../Validator/mixin_login_validator.dart';
// import '../bloc/bloc.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'images',
      home: Scaffold(
        appBar: AppBar(
          title: const Text("image per seconds"),
        ),
        resizeToAvoidBottomInset: false,
        body: ImagesScreen(),
      ),
    );
  }
}

// class ImagesListScreen extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return MessagesState();
//   }
// }

class ImagesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ImagesScreenState();
  }
}

class ImagesScreenState extends State<StatefulWidget> {
  ImagesStream imagesStream = ImagesStream();
  final imgs = <ImageRef>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: imagesStream.getImages(),
        builder: (context, AsyncSnapshot<ImageRef> snapshot) {
          // if (snapshot.connectionState == ConnectionState.waiting) {
          //   return const CircularProgressIndicator();
          // }
          if (snapshot.hasData) {
            print(snapshot.data!.title);
            imgs.add(snapshot.data!);
            return ListView.builder(
              itemCount: imgs.length,
              itemBuilder: (context, index) {
                final chatItem = imgs[index].title;
                final url = imgs[index].url!;
                print(url);
                return ListTile(
                  title: Text(
                    chatItem!,
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  leading: Image.network("$url"),
                );
              },
            );
          } else {
            return const LinearProgressIndicator();
          }
        });
  }
}
